import BookDetails from 'pages/books/BookDetails';

export const privateRoutes = [{ link: '/:id', element: <BookDetails /> }];

export const publicRoutes = [];
