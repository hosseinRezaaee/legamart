import 'App.css';
import React, { useState } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { Box } from '@mui/system';

import { privateRoutes } from 'routes/routes';
import { useAuth } from 'hooks/Auth';
import Login from 'pages/login/Login';
import Books from 'pages/books/Books';
import Snack from 'components/snackbar/Snack';
import { User, OneBookDetail, AllBooks, SnackbarCtx } from 'constants/context';

const App = () => {
  const { isAuth } = useAuth();
  const [user, setUser] = useState();
  const [allBooks, setAllBooks] = useState(null);
  const [bookDetail, setBookDetail] = useState();
  const [snackbarMsg, setSnackbarMsg] = useState(null);

  return (
    <>
      <Box className="container" width="1">
        <SnackbarCtx.Provider value={{ snackbarMsg, setSnackbarMsg }}>
          <Snack />
          <User.Provider value={{ user, setUser }}>
            <AllBooks.Provider value={{ allBooks, setAllBooks }}>
              <OneBookDetail.Provider value={{ bookDetail, setBookDetail }}>
                <Routes>
                  <Route
                    path="/"
                    element={isAuth() ? <Books /> : <Navigate to="/login" />}
                  />
                  <Route
                    path="/login"
                    element={isAuth() ? <Navigate to="/" /> : <Login />}
                  />
                  {isAuth() ? (
                    privateRoutes.map((pr) => (
                      <Route
                        key={pr.link}
                        path={pr.link}
                        element={pr.element}
                      />
                    ))
                  ) : (
                    <Route path="*" element={<Navigate to="/login" />} />
                  )}
                </Routes>
              </OneBookDetail.Provider>
            </AllBooks.Provider>
          </User.Provider>
        </SnackbarCtx.Provider>
      </Box>
    </>
  );
};

export default App;
