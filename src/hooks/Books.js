import { useEffect, useState } from 'react';

import { useStopMemoryLeak } from './StopMemoryLeak';
import { product } from 'helper/fetch';
import { useReq } from 'hooks/Req';

export const useBooks = (books = null) => {
  const [booksData, setBooksData] = useState(null);
  const { mounted } = useStopMemoryLeak();
  const { req } = useReq();

  const fetchBooks = () => {
    !books
      ? req(product.getBooks())
          .then((res) => {
            setBooksData(res.data.payload);
          })
          .catch((err) => {
            setBooksData(false);
          })
      : setBooksData(books);
  };

  useEffect(() => {
    if (mounted) fetchBooks();
  }, [mounted]);

  return {
    booksData,
  };
};
