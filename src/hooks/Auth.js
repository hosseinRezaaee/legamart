import { useContext } from 'react';

import { User } from 'constants/context';

export const useAuth = () => {
  const { setUser } = useContext(User);

  const isAuth = () => {
    return Boolean(localStorage.getItem('loggedin'));
  };

  const login = (value) => {
    localStorage.setItem('loggedin', true);
    setUser(value);
  };

  const logout = () => {
    localStorage.removeItem('loggedin');
    setUser(null);
  };

  return {
    isAuth,
    login,
    logout,
  };
};
