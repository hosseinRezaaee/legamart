import { useContext } from 'react';
import { SnackbarCtx } from 'constants/context';

export const useReq = () => {
  const { setSnackbarMsg } = useContext(SnackbarCtx);

  const req = (req) => {
    return req
      .then((res) => Promise.resolve(res))
      .catch((err) => {
        setSnackbarMsg(err);
        return Promise.reject(err);
      });
  };

  return {
    req,
  };
};
