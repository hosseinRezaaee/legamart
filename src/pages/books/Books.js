import React, { useContext, useEffect } from 'react';

import { useBooks } from 'hooks/Books';
import layoutHOC from 'layout';
import { Box, CircularProgress } from '@mui/material';
import BooksWrapper from 'components/books/BooksWrapper';
import { AllBooks } from 'constants/context';

const Books = () => {
  const { allBooks, setAllBooks } = useContext(AllBooks);
  const { booksData } = useBooks(allBooks);

  useEffect(() => {
    if (booksData) setAllBooks(booksData);
  }, [booksData]);

  return (
    <Box width="1" display="flex" justifyContent="center">
      {booksData ? (
        <BooksWrapper />
      ) : booksData === null ? (
        <Box mt={10}>
          <CircularProgress />
        </Box>
      ) : null}
      {/*☝ if useBooks returned error show nothing ☝*/}
    </Box>
  );
};

export default layoutHOC(Books);
