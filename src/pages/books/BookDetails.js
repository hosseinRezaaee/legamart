import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import {
  Container,
  Box,
  Avatar,
  CircularProgress,
  Chip,
  Typography,
  Paper,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import layoutHOC from 'layout';
import { AllBooks, OneBookDetail } from 'constants/context';
import { useBooks } from 'hooks/Books';

export const useStyles = makeStyles({
  container: {
    paddingTop: '20px',
    paddingBottom: '100px',
    margin: '30px auto',
  },
  avatar: {
    width: '200px',
    height: '200px',
  },
});
const BookDetails = () => {
  const { id } = useParams();
  const { allBooks, setAllBooks } = useContext(AllBooks);
  const { bookDetail, setBookDetail } = useContext(OneBookDetail);
  const [shownBook, setShownBook] = useState();
  const classes = useStyles();

  const { booksData } = useBooks(allBooks);
  useEffect(() => {
    if (booksData) setAllBooks(booksData);
  }, [booksData]);

  useEffect(() => {
    if (String(bookDetail?.id) === id) {
      setShownBook(bookDetail);
    } else {
      if (booksData) {
        const temp = booksData?.filter((book) => String(book.id) === id)[0];
        setShownBook(temp);
        setBookDetail(temp);
      }
    }
  }, [booksData]);

  return (
    <>
      {shownBook ? (
        <Container component={Paper} className={classes.container}>
          <Box width="1" dir="rtl" mt={4}>
            <Box width="1" display="flex" alignItems="center">
              <Avatar className={classes.avatar} src={shownBook.picture} />
              <Box mr={5} display="flex">
                {shownBook.tags?.map((tag, i) => (
                  <Box key={tag + shownBook.title + i} ml={1} flexWrap="wrap">
                    <Chip label={tag} color="success" variant="outlined" />
                  </Box>
                ))}
              </Box>
            </Box>
            <Box mt={4}>
              <Typography variant="span" color="GrayText">
                {shownBook.createdAt &&
                  new Date(shownBook.createdAt).toLocaleDateString('en-US', {
                    weekday: 'short',
                    day: 'numeric',
                    year: 'numeric',
                    hour: '2-digit',
                    hour12: false,
                    minute: '2-digit',
                  })}
              </Typography>
            </Box>
            <Box mt={3}>
              <Typography variant="h3">{shownBook.title}</Typography>
            </Box>
            <Box mt={5}>
              <Typography variant="p">{shownBook.description}</Typography>
            </Box>
          </Box>
        </Container>
      ) : (
        <Box width="1" mt={4} display="flex" justifyContent="center">
          <CircularProgress />
        </Box>
      )}
    </>
  );
};

export default layoutHOC(BookDetails);
