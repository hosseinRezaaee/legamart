import React from 'react';
import { Box, Button, Grid } from '@mui/material';

import Input from 'components/form/input/Input';
import { registraition } from 'helper/fetch';
import { useAuth } from 'hooks/Auth';
import { useReq } from 'hooks/Req';

const Login = () => {
  const { login } = useAuth();
  const { req } = useReq();
  const submitLoginForm = (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const username = formData.get('username');
    const password = formData.get('password');
    req(registraition.login({ username, password })).then((res) => {
      login(res.data.payload);
    });
  };

  return (
    <Box
      width="1"
      height="100vh"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <Box component={Grid} item md={4} xs={10}>
        <Box component="form" width="1" onSubmit={submitLoginForm}>
          <Box>
            <Input type="text" name="username" label="username" fullWidth />
          </Box>
          <Box mt={4}>
            <Input type="password" name="password" label="password" fullWidth />
          </Box>
          <Box mt={12}>
            <Button type="submit" variant="contained" fullWidth>
              login
            </Button>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default Login;
