import React, { useContext, useState } from 'react';
import { Box, Button, Chip, Container, Grid, Stack } from '@mui/material';

import Togglebutton from 'components/form/toggleButton/Togglebutton';
import CustomTable from 'components/table/CustomTable';
import { Link } from 'react-router-dom';
import SimpleCard from 'components/card/SimpleCard';
import { useNavigate } from 'react-router-dom';
import { OneBookDetail } from 'constants/context';

const btns = [
  { text: 'table', value: 'table' },
  { text: 'grid', value: 'grid' },
];

const BooksList = ({ data }) => {
  const { setBookDetail } = useContext(OneBookDetail);
  const [view, setView] = useState('table');
  const navigate = useNavigate();
  const changeView = (e, x) => {
    x && setView(x);
  };

  return (
    <Box width="1">
      <Box width="1" display="flex" justifyContent="center" my={4}>
        <Togglebutton
          value={view}
          onChange={changeView}
          btns={btns}
          exclusive
        />
      </Box>
      <Box
        width="1"
        display="flex"
        justifyContent="center"
        alignItems="center"
        mb={10}
      >
        {data?.length && (
          <Container>
            {view === 'grid' ? (
              <Grid container spacing={2}>
                {data.map((book) => (
                  <SimpleCard
                    key={book.id}
                    img={book.picture}
                    title={book.title}
                    summary={book.description}
                    action={{
                      text: 'details',
                      click: () => {
                        setBookDetail(book);
                        navigate(`/${book.id}`);
                      },
                    }}
                  />
                ))}
              </Grid>
            ) : (
              <CustomTable
                headers={['title', 'tags', 'created time', 'link']}
                rows={[
                  ...data.map((book) => [
                    book.title,
                    <Stack direction="row" spacing={1}>
                      {book.tags?.map((tag, i) => (
                        <Chip
                          key={tag + book.title + i}
                          label={tag}
                          color="success"
                          variant="outlined"
                        />
                      ))}
                    </Stack>,
                    book.createdAt &&
                      new Date(book.createdAt).toLocaleDateString('en-US', {
                        weekday: 'short',
                        day: 'numeric',
                        year: 'numeric',
                        hour: '2-digit',
                        hour12: false,
                        minute: '2-digit',
                      }),
                    <Link to={`/${book.id}`}>
                      <Button variant="outlined">details</Button>
                    </Link>,
                  ]),
                ]}
                align="left"
              />
            )}
          </Container>
        )}
      </Box>
    </Box>
  );
};

export default BooksList;
