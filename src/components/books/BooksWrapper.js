import React, { useContext, useEffect, useMemo, useState } from 'react';
import { Box, AppBar, Toolbar } from '@mui/material';

import { AllBooks } from 'constants/context';
import Input from 'components/form/input/Input';
import AutoComplete from 'components/form/autoComplete/AutoComplete';
import BooksList from 'components/books/BooksList';

const BooksWrapper = () => {
  const { allBooks } = useContext(AllBooks);
  const [books, setBooks] = useState({
    search: allBooks || [],
    tags: allBooks || [],
    shown: allBooks || [],
  });
  const searching = (e) => {
    const val = e.target.value;
    if (val !== '') {
      const temp = allBooks.filter((book) => book.title.includes(val));
      setBooks({ ...books, search: temp });
    } else {
      setBooks({ ...books, search: allBooks });
    }
  };

  useEffect(() => {
    let arr = [...books.tags, ...books.search];
    let sorted_arr = arr
      .map((book) => book.id)
      .slice()
      .sort();

    let results = [];
    for (let i = 0; i < sorted_arr.length - 1; i++) {
      if (sorted_arr[i + 1] === sorted_arr[i]) {
        results.push(sorted_arr[i]);
      }
    }
    setBooks({
      ...books,
      shown: allBooks?.filter((book) => results.indexOf(book.id) > -1),
    });
  }, [books.tags, books.search]);

  let tagChecker = (book, tags) => tags?.every((tag) => book?.includes(tag));

  const tagMaping = (e, tags) => {
    if (tags.length) {
      const minfyTags = tags.map((tag) => tag.title);
      const taggedBooks = allBooks
        .map((book) => (tagChecker(book.tags, minfyTags) ? book : null))
        .filter((book) => book?.id);
      setBooks({ ...books, tags: taggedBooks });
    } else {
      setBooks({ ...books, tags: allBooks });
    }
  };

  useEffect(() => {
    allBooks &&
      setBooks({
        search: allBooks,
        tags: allBooks,
        shown: allBooks,
      });
  }, [allBooks]);

  const tags = useMemo(() => {
    const temp = [];
    allBooks?.map((book) => book?.tags?.map((tag) => temp.push(tag)));
    const options = [...new Set(temp)].map((option) => ({
      value: option,
      title: option,
    }));
    return [...options];
  }, [allBooks]);

  return (
    <Box width="1">
      <AppBar position="static" color="default">
        <Box
          component={Toolbar}
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          flexWrap="wrap"
          py={1}
        >
          <Box width="30%" minWidth="300px">
            <Input
              fullWidth
              name="search"
              label="search"
              onChange={searching}
            />
          </Box>
          <Box width="30%" minWidth="300px">
            <AutoComplete
              options={tags}
              onChange={tagMaping}
              placeholder="select..."
              label="tags"
            />
          </Box>
        </Box>
      </AppBar>
      <Box>
        <BooksList data={books.shown} />
      </Box>
    </Box>
  );
};

export default BooksWrapper;
