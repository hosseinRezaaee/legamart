import React, { useContext, useEffect, useState } from 'react';
import { Snackbar, Box, Button } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { SnackbarCtx } from 'constants/context';

const useStyles = makeStyles({
  root: {
    '& div': {
      background: 'orange',
    },
  },
});

const Snack = () => {
  const { snackbarMsg, setSnackbarMsg } = useContext(SnackbarCtx);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setOpen(snackbarMsg);
  }, [snackbarMsg]);

  const classes = useStyles();

  const close = () => {
    setOpen(false);
    setSnackbarMsg(null);
  };
  return (
    <Box>
      <Snackbar
        className={classes.root}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={Boolean(open)}
        autoHideDuration={2000}
        onClose={close}
        message={open}
        action={
          <Button onClick={close} variant="contained" color="error">
            close
          </Button>
        }
      />
    </Box>
  );
};

export default Snack;
