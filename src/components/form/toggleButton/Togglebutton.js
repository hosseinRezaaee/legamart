import React from 'react';
import { ToggleButtonGroup, ToggleButton } from '@mui/material';

const Togglebutton = (props) => {
  const { onChange, value, btns, exclusive, ...rest } = props;
  return (
    <ToggleButtonGroup {...{ onChange, value, exclusive, ...rest }}>
      {btns.map((btn) => (
        <ToggleButton key={btn.value} value={btn.value}>
          {btn.text}
        </ToggleButton>
      ))}
    </ToggleButtonGroup>
  );
};

export default Togglebutton;
