import React from 'react';
import { TextField } from '@mui/material';

const Input = (props) => {
  const {
    name,
    label,
    type = 'string',
    disabled = false,
    onChange,
    value,
    defaultValue,
    ...rest
  } = props;
  return (
    <TextField
      variant="standard"
      {...{
        name,
        label,
        type,
        disabled,
        onChange,
        value,
        defaultValue,
        ...rest,
      }}
    />
  );
};

export default Input;
