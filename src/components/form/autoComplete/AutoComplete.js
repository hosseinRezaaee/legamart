import React from 'react';
import { Autocomplete, TextField } from '@mui/material';

const AutoComplete = (props) => {
  const {
    options,
    placeholder,
    label,
    defaultValue,
    onChange,
    variant = 'standard',
  } = props;
  return (
    <Autocomplete
      multiple
      options={options || []}
      getOptionLabel={(option) => option?.title}
      renderInput={(params) => (
        <TextField {...{ placeholder, label, variant, ...params }} />
      )}
      {...{ defaultValue, onChange }}
    />
  );
};

export default AutoComplete;
