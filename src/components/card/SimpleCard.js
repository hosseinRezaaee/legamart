import React from 'react';
import {
  Button,
  Card,
  CardActions,
  CardMedia,
  Box,
  Grid,
  Typography,
} from '@mui/material';
import { useStyles } from './SimpleCard.style';

const SimpleCard = (props) => {
  const classes = useStyles();
  const { sizing = { md: 3, xs: 6 }, img, title, summary, action } = props;
  return (
    <Grid item md={sizing.md} xs={sizing.xs} dir="rtl">
      <Card>
        <CardMedia component="img" alt="image" height="200" image={img} />
        <Box p={2} className={classes.root}>
          <Box className={classes.shadow} />
          <Typography gutterBottom variant="h5" component="div">
            {title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {summary}
          </Typography>
        </Box>
        <CardActions>
          <Button variant="outlined" fullWidth onClick={action.click}>
            {action.text}
          </Button>
        </CardActions>
      </Card>
    </Grid>
  );
};

export default SimpleCard;
