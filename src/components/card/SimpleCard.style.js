import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
  root: {
    height: '170px',
    overflow: 'hidden',
    position: 'relative',
  },
  shadow: {
    background: 'linear-gradient( transparent 0%,#fff 100%)',
    position: 'absolute',
    bottom: '0',
    left: '0',
    width: '100%',
    height: '100px',
  },
});
