import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:9000/';

axios.interceptors.response.use(
  (res) => {
    if (res) {
      return Promise.resolve(res);
    }
  },
  (err) => {
    if (err?.response?.status !== 200) {
      return Promise.reject(err.response.data.message);
    }
    return Promise.reject(err);
  }
);

export const registraition = {
  login: async (data) => {
    return await axios.post(`auth/login`, data);
  },
};

export const product = {
  getBooks: async () => {
    return await axios.get(`books`);
  },
};
