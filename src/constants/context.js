import { createContext } from 'react';

export const User = createContext({});
export const OneBookDetail = createContext({});
export const AllBooks = createContext(null);
export const SnackbarCtx = createContext(null);
