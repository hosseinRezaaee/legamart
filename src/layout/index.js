import React, { useContext } from 'react';
import { Box, AppBar, Toolbar, Button, Typography } from '@mui/material';

import { User } from 'constants/context';
import { useAuth } from 'hooks/Auth';

const layoutHOC = (Page) => {
  return () => {
    const { user } = useContext(User);
    const { logout } = useAuth();
    return (
      <Box width="1">
        <AppBar position="static">
          <Toolbar>
            <Box flexGrow="1">
              <Button color="inherit" variant="outlined" onClick={logout}>
                logout
              </Button>
            </Box>
            <Box>
              <Typography>{user?.fullName}</Typography>
            </Box>
          </Toolbar>
        </AppBar>
        <Box component="main">
          <Page />
        </Box>
      </Box>
    );
  };
};

export default layoutHOC;
